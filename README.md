﻿# Resultado do Teste para programador PHP

## Execução do projeto

### Requisitos:

	* PHP 7
	* Symfony 3.2 (com Doctrine)
	* MySQL

### Passos:

	1. Banco de dados: Executar os arquivos do diretório vox/sql

	2. Ambiente: no Prompt, caminhar até a pasta "vox" e executar o comando: > php bin/console server:run

	3. Por padrão o ambiente Symfony está configurado para "localhost:8000"