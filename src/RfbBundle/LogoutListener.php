<?php

namespace RfbBundle;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;

class LogoutListener implements LogoutSuccessHandlerInterface {

    public function onLogoutSuccess(Request $request) {
        header('Location: http://logout:logout@' . $_SERVER['HTTP_HOST']);
        exit;
    }

}
