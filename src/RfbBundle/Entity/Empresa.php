<?php

namespace RfbBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Empresa
 *
 * @ORM\Table(name="empresa")
 * @ORM\Entity(repositoryClass="RfbBundle\Repository\EmpresaRepository")
 */
class Empresa {

    public function __construct($id = null) {
        $this->id = $id;
        $this->socios = new ArrayCollection();
    }

    /**
     * @ORM\ManyToMany(targetEntity="Socio",cascade={"persist"})
     * @ORM\JoinTable(name="empresa_socio",
     *      joinColumns={@ORM\JoinColumn(name="id_empresa", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="id_socio", referencedColumnName="id", unique=true)}
     * )
     */
    private $socios;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\Length(min = 14, max = 14, exactMessage="O CNPJ deve conter 14 dígitos")
     * @Assert\Regex(pattern = "/^\d{14}/", message = "O CNPJ deve conter somente dígitos")
     * @ORM\Column(name="cnpj", type="string", length=14, unique=true)
     */
    private $cnpj;

    /**
     * @var string
     * @Assert\NotBlank(message = "O nome não pode ficar vazio")
     * @ORM\Column(name="nome", type="string", length=100)
     */
    private $nome;

    /**
     * Recupera o ID
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Seta o CNPJ
     *
     * @param string $cnpj
     *
     * @return Empresa
     */
    public function setCnpj($cnpj) {
        $this->cnpj = $cnpj;

        return $this;
    }

    /**
     * Recupera o CNPJ
     *
     * @param bool $format [optional]
     * @return string
     */
    public function getCnpj($format = false) {
        $cnpj = $this->cnpj;
        if ($format) {
            $cnpj = substr($this->cnpj, 0, 2) . '.' .
                    substr($this->cnpj, 2, 3) . '.' .
                    substr($this->cnpj, 5, 3) . '/' .
                    substr($this->cnpj, 8, 4) . '-' .
                    substr($this->cnpj, 12);
        }
        return $cnpj;
    }

    /**
     * Seta o nome
     *
     * @param string $nome
     *
     * @return Empresa
     */
    public function setNome($nome) {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Recupera o nome
     *
     * @return string
     */
    public function getNome() {
        return $this->nome;
    }

    public function addSocio(Socio $s) {
        if (!$this->socios->contains($s)) {
            $this->socios->add($s);
        }
    }

    /**
     * Recupera a lista de socios
     *
     * @return ArrayCollection
     */
    public function getSocios() {
        return $this->socios;
    }

    /**
     * Remove um socio desta empresa
     * @param Socio $s o socio a ser removido
     */
    public function removeSocio(Socio $s) {
        if (!$this->socios->contains($s)) {
            return;
        }
        $this->socios->removeElement($s);
        $s->removeEmpresa($this);
    }

}
