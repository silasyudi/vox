<?php

namespace RfbBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Socio
 *
 * @ORM\Table(name="socio")
 * @ORM\Entity(repositoryClass="RfbBundle\Repository\SocioRepository")
 */
class Socio {

    public function __construct() {
        $this->empresas = new ArrayCollection();
    }

    /**
     * @ORM\ManyToMany(targetEntity="Empresa",cascade={"persist"})
     * @ORM\JoinTable(name="empresa_socio",
     *      joinColumns={@ORM\JoinColumn(name="id_socio", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="id_empresa", referencedColumnName="id", unique=true)}
     * )
     */
    private $empresas;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\Length(min = 11, max = 11, exactMessage="O CPF deve conter 11 dígitos")
     * @Assert\Regex(pattern = "/^\d{11}/", message = "O CPF deve conter somente dígitos")
     * @ORM\Column(name="cpf", type="string", length=11, unique=true)
     */
    private $cpf;

    /**
     * @var string
     * @Assert\NotBlank(message = "O nome não pode ficar vazio")
     * @ORM\Column(name="nome", type="string", length=100)
     */
    private $nome;

    /**
     * @var string
     * @Assert\Email(message = "O e-mail '{{ value }}' não é um endereço de e-mail válido.")
     * @ORM\Column(name="email", type="string", length=100, unique=true)
     */
    private $email;

    /**
     * Recupera o ID
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Seta o CPF
     *
     * @param string $cpf
     *
     * @return Socio
     */
    public function setCpf($cpf) {
        $this->cpf = $cpf;

        return $this;
    }

    /**
     * Recupera o CPF
     *
     * @return string
     */
    public function getCpf($format = false) {
        $cpf = $this->cpf;
        if ($format) {
            $cpf = substr($this->cpf, 0, 3) . '.' .
                    substr($this->cpf, 3, 3) . '.' .
                    substr($this->cpf, 6, 3) . '-' .
                    substr($this->cpf, 9);
        }
        return $cpf;
    }

    /**
     * Seta o nome
     *
     * @param string $nome
     *
     * @return Socio
     */
    public function setNome($nome) {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Recupera o nome
     *
     * @return string
     */
    public function getNome() {
        return $this->nome;
    }

    /**
     * Seta o E-mail
     *
     * @param string $email
     *
     * @return Socio
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Recupera o E-mail
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Adiciona uma empresa ao socio
     * @param \RfbBundle\Entity\Empresa $e A empresa a ser adicionada
     */
    public function addEmpresa(Empresa $e) {
        if (!$this->empresas->contains($e)) {
            $this->empresas->add($e);
        }
    }

    /**
     * Limpa a lista de empresas
     */
    public function clearEmpresas() {
        $this->empresas->clear();
    }

    /**
     * Recupera a lista de empresas da qual este socio faz parte
     *
     * @return ArrayCollection
     */
    public function getEmpresas() {
        return $this->empresas;
    }

    /**
     * Remove uma empresa deste socio
     * @param \RfbBundle\Entity\Empresa $e A empresa a ser removida
     */
    public function removeEmpresa(Empresa $e) {
        if (!$this->empresas->contains($e)) {
            return;
        }
        $this->empresas->removeElement($e);
        $e->removeSocio($this);
    }

}
