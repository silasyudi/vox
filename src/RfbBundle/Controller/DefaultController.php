<?php

namespace RfbBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller {

    /**
     * Exibe a pagina inicial
     *
     * @Route("/", name="app_index")
     */
    public function index() {
        return $this->render('index.html.twig');
    }

}
