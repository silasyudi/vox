<?php

namespace RfbBundle\Controller;

use RfbBundle\Entity\Socio;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Socio controller
 *
 * @Route("socio")
 */
class SocioController extends Controller {

    /**
     * Exibe todos os socios
     *
     * @Route("/", name="socio_index")
     * @Method("GET")
     */
    public function indexAction() {
        $socios = $this->getDoctrine()->getRepository('RfbBundle:Socio')->findBy(array(), array('nome' => 'asc', 'cpf' => 'asc'));
        return $this->render('socio/index.html.twig', array('socios' => $socios));
    }

    /**
     * Exibe um formulario de criacao de socio e controla a persistencia do novo socio
     *
     * @Route("/new", name="socio_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        if ($request->getMethod() == 'POST') {
            $socio = $this->save($request, new Socio());
            if (!empty($socio)) {
                return $this->redirectToRoute('socio_show', array('id' => $socio->getId()));
            }
        }

        return $this->render('socio/new.html.twig');
    }

    /**
     * Exibe um socio
     *
     * @Route("/{id}", name="socio_show")
     * @Method("GET")
     */
    public function showAction(Socio $socio) {
        return $this->render('socio/show.html.twig', array('socio' => $socio));
    }

    /**
     * Exibe um formulario de edicao e controla a persistencia da edicao
     *
     * @Route("/{id}/edit", name="socio_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Socio $socio) {
        if ($request->getMethod() == 'POST') {
            $s = $this->save($request, $socio);
            if (!empty($s)) {
                return $this->redirectToRoute('socio_show', array('id' => $s->getId()));
            }
        }

        return $this->render('socio/edit.html.twig', array('socio' => $socio));
    }

    /**
     * Procura um socio pelo CPF e empresa da qual faz parte
     *
     * @param string $cpf [optional] Um numero de CPF parcial ou completo
     * @param int $emp [optional] O ID da empresa
     * @return string JSON contendo em cada objeto campos url, title, description, name e value
     * @Route("/search/{cpf}/{emp}", name="socio_search_cpf")
     */
    public function searchCPFAction($cpf = '', $emp = null) {
        $em = $this->getDoctrine()->getRepository('RfbBundle:Socio');
        $qb = $em->createQueryBuilder('s')
                ->where('s.cpf LIKE :c')
                ->setParameter('c', '%' . $cpf . '%');

        $socios = $qb->getQuery()->getResult();

        if (!is_null($emp)) {
            $empresa = $this->getDoctrine()->getManager()->find('RfbBundle:Empresa', $emp);
        }

        $r = array();
        foreach ($socios as $ss) {
            if (!is_null($emp) && !$ss->getEmpresas()->contains($empresa)) {
                continue;
            }

            $s = new \stdClass();
            //Para pesquisa de socios
            $s->url = $this->generateUrl('socio_show', array('id' => $ss->getId()));
            $s->title = $ss->getNome();
            $s->description = $ss->getCpf(true);
            //Para acrescentar a empresas
            $s->name = $ss->getNome();
            $s->value = $ss->getId();
            $r[] = $s;
        }

        $o = new \stdClass();
        $o->success = true;
        $o->results = $r;

        return new \Symfony\Component\HttpFoundation\JsonResponse($o);
    }

    /**
     * Exclui um socio
     *
     * @Route("/{id}/delete", name="socio_delete")
     */
    public function deleteAction(Request $request, Socio $socio) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($socio);
        $em->flush();

        return $this->redirectToRoute('socio_index');
    }

    /**
     * Salva um socio na persistencia
     *
     * @param Request $request a requisicao
     * @param Socio $socio o socio a ser salvo
     * @return mixed FALSE, se houver campos unicos ja existentes ou o objeto $socio, se salvar corretamente
     */
    private function save(Request $request, Socio $socio) {
        $data = $request->request->all();

        //Preenche o objeto
        $socio->setNome($data['nome']);
        $socio->setCpf('a' . $data['cpf']);
        $socio->setEmail($data['email']);

        //Validacao dos campos
        $validator = $this->get('validator');
        $errors = $validator->validate($socio);
        if (count($errors) > 0) {
            $session = new \Symfony\Component\HttpFoundation\Session\Session();
            foreach ($errors as $ee) {
                $session->getFlashBag()->add('error', $ee->getMessage());
            }
            return false;
        }

        //Verifica email e CPF unicos
        $email = null;
        if (strcmp($socio->getEmail(), $data['email']) !== 0) {
            $email = $this->getDoctrine()->getRepository('RfbBundle:Socio')->findBy(array('email' => $data['email']));
        }

        $cpf = null;
        if (strcmp($socio->getCpf(), $data['cpf']) !== 0) {
            $cpf = $this->getDoctrine()->getRepository('RfbBundle:Socio')->findBy(array('cpf' => $data['cpf']));
        }

        if (!empty($email) || !empty($cpf)) {
            $message = 'Já existe sócio cadastrado com os seguintes dados: ';
            $message .= $email ? '[E-mail] ' : '';
            $message .= $cpf ? '[CPF]' : '';

            $session = new \Symfony\Component\HttpFoundation\Session\Session();
            $session->getFlashBag()->add('error', $message);

            return false;
        }

        //Insere as empresas
        $socio->clearEmpresas();

        $emp = explode(',', $data['empresa']);
        foreach ($emp as $e) {
            $empresa = $this->getDoctrine()->getManager()->find('RfbBundle:Empresa', $e);
            if (!empty($empresa)) {
                $socio->addEmpresa($empresa);
            }
        }

        //Persiste
        $this->getDoctrine()->getManager()->persist($socio);
        $this->getDoctrine()->getManager()->flush();

        return $socio;
    }

}
