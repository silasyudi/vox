<?php

namespace RfbBundle\Controller;

use RfbBundle\Entity\Empresa;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Empresa controller
 *
 * @Route("empresa")
 */
class EmpresaController extends Controller {

    /**
     * Exibe todas as empresas
     *
     * @Route("/", name="empresa_index")
     * @Method("GET")
     */
    public function indexAction() {
        $empresas = $this->getDoctrine()->getRepository('RfbBundle:Empresa')->findBy(array(), array('nome' => 'asc', 'cnpj' => 'asc'));
        return $this->render('empresa/index.html.twig', array('empresas' => $empresas));
    }

    /**
     * Cria uma nova empresa
     *
     * @Route("/new", name="empresa_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        if ($request->getMethod() == 'POST') {
            $empresa = $this->save($request, new Empresa());
            if (!empty($empresa)) {
                return $this->redirectToRoute('empresa_show', array('id' => $empresa->getId()));
            }
        }

        return $this->render('empresa/new.html.twig');
    }

    /**
     * Exibe uma empresa
     *
     * @Route("/{id}", name="empresa_show")
     * @Method("GET")
     */
    public function showAction(Empresa $empresa) {
        return $this->render('empresa/show.html.twig', array('empresa' => $empresa));
    }

    /**
     * Exibe um formulario de edicao e controla a persistencia da edicao
     *
     * @Route("/{id}/edit", name="empresa_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Empresa $empresa) {
        if ($request->getMethod() == 'POST') {
            $e = $this->save($request, $empresa);
            if (!empty($e)) {
                return $this->redirectToRoute('empresa_show', array('id' => $e->getId()));
            }
        }

        return $this->render('empresa/edit.html.twig', array('empresa' => $empresa));
    }

    /**
     * Exclui uma empresa
     *
     * @Route("/{id}/delete", name="empresa_delete")
     */
    public function deleteAction(Empresa $empresa) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($empresa);
        $em->flush();

        return $this->redirectToRoute('empresa_index');
    }

    /**
     * Acrescenta um socio a uma empresa
     *
     * @Route("/{id}/add/", name="empresa_novo_socio")
     */
    public function addSocioAction(Request $request, Empresa $empresa) {
        $data = $request->request->all();

        $soc = explode(',', $data['socio']);
        foreach ($soc as $s) {
            $socio = $this->getDoctrine()->getManager()->find('RfbBundle:Socio', $s);
            $empresa->addSocio($socio);
        }

        $this->getDoctrine()->getManager()->persist($empresa);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('empresa_show', array('id' => $empresa->getId()));
    }

    /**
     * Remove um socio de uma empresa
     *
     * @Route("/{id}/remove/", name="empresa_remove_socio")
     */
    public function removeSocioAction(Request $request, Empresa $empresa) {
        $data = $request->request->all();
        $socio = $this->getDoctrine()->getRepository('RfbBundle:Socio')->find($data['socio']);
        $empresa->removeSocio($socio);

        $this->getDoctrine()->getManager()->persist($empresa);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('empresa_show', array('id' => $empresa->getId()));
    }

    /**
     * Procura uma empresa pelo seu CNPJ
     *
     * @param string CNPJ o CNPJ parcial ou completo de uma empresa
     * @return string JSON contendo em cada objeto campos url, title, description, name e value
     * @Route("/search/{cnpj}", name="empresa_search_cnpj")
     */
    public function searchCNPJAction($cnpj = '') {
        $em = $this->getDoctrine()->getRepository('RfbBundle:Empresa');
        $qb = $em->createQueryBuilder('e')
                ->where('e.cnpj LIKE :c')
                ->setParameter('c', '%' . $cnpj . '%');

        $empresas = $qb->getQuery()->getResult();

        $r = array();
        foreach ($empresas as $ee) {
            $e = new \stdClass();
            //Para pesquisa de empresas
            $e->url = $this->generateUrl('empresa_show', array('id' => $ee->getId()));
            $e->title = $ee->getNome();
            $e->description = $ee->getCnpj(true);
            //Para criar novos socios
            $e->name = $ee->getNome();
            $e->value = $ee->getId();
            $r[] = $e;
        }

        $o = new \stdClass();
        $o->success = true;
        $o->results = $r;

        return new \Symfony\Component\HttpFoundation\JsonResponse($o);
    }

    /**
     * Salva uma empresa na persistencia
     *
     * @param Request $request a requisicao
     * @param Empresa $empresa a empresa a ser salva
     * @return mixed FALSE, se houver campos unicos ja existentes ou o objeto $empresa, se salvar corretamente
     */
    private function save(Request $request, Empresa $empresa) {
        $data = $request->request->all();

        //Preenche objeto
        $empresa->setNome($data['nome']);
        $empresa->setCnpj($data['cnpj']);

        //Validacao dos campos
        $validator = $this->get('validator');
        $errors = $validator->validate($empresa);
        if (count($errors) > 0) {
            $session = new \Symfony\Component\HttpFoundation\Session\Session();
            foreach ($errors as $ee) {
                $session->getFlashBag()->add('error', $ee->getMessage());
            }
            return false;
        }

        //Verifica CNPJ unico
        $cnpj = null;
        if (strcmp($empresa->getCnpj(), $data['cnpj']) !== 0) {
            $cnpj = $this->getDoctrine()->getRepository('RfbBundle:Empresa')->findBy(array('cnpj' => $data['cnpj']));
        }

        if (!empty($cnpj)) {
            $message = 'Já existe empresa cadastrada com os seguintes dados: [CNPJ]';

            $session = new \Symfony\Component\HttpFoundation\Session\Session();
            $session->getFlashBag()->add('error', $message);

            return false;
        }

        //Persiste
        $this->getDoctrine()->getManager()->persist($empresa);
        $this->getDoctrine()->getManager()->flush();

        return $empresa;
    }

}
