/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

DELETE FROM `empresa`;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
INSERT INTO `empresa` (`id`, `cnpj`, `nome`) VALUES
	(1, '32595160412906', 'Morbi At & Cia'),
	(2, '70298501000216', 'Quis Mollis Ltda'),
	(3, '18941617967111', 'Posuere Magna Ltda'),
	(4, '54398143216438', 'Dui Vehicula, Grupo'),
	(5, '55020402036038', 'Quisque Sed Ltda'),
	(6, '28397137042502', 'Molestie Leo Ltda'),
	(7, '72772644374774', 'Consequat Imperdiet, Grupo'),
	(8, '50229807774903', 'Molestie Sagittis Ltda'),
	(9, '47113889229011', 'Pellentesque Integer Ltda'),
	(10, '67100051270079', 'Pulvinar Nulla Ltda'),
	(11, '44077838803993', 'Augue Eget'),
	(12, '20745472982117', 'Sed Vel Ltda'),
	(13, '14940585762095', 'Ornare Sodales'),
	(14, '47326681281928', 'Auctor Molestie S/A'),
	(15, '89898333304498', 'Odio Sem & Cia'),
	(16, '01352851027733', 'Ex Ac'),
	(17, '90388311091460', 'Lobortis Est & Cia'),
	(18, '61332718200700', 'Leo Sed & Cia'),
	(19, '16481625871436', 'Eget Mauris Ltda'),
	(20, '45285893718296', 'Sagittis Orci & Cia'),
	(21, '72322442078257', 'Sapien Tempor Ltda'),
	(22, '79847867533829', 'Risus Non'),
	(23, '93708659081726', 'Pharetra Nibh, Grupo'),
	(24, '98720602500679', 'Diam Feugiat Ltda'),
	(25, '94522779139275', 'Proin Ac S/A'),
	(26, '95116812303733', 'Et Nulla'),
	(27, '38395002011635', 'Auctor Leo & Cia'),
	(28, '31110848024207', 'Tortor Vulputate, Grupo'),
	(29, '42280497267247', 'In Dictum, Grupo'),
	(30, '68961198029647', 'Viverra Sed & Cia'),
	(31, '17934826637981', 'Laoreet Mi, Grupo'),
	(32, '71754504635963', 'Tellus Amet & Cia'),
	(33, '61967206788770', 'Non Sit Ltda'),
	(34, '92666588812190', 'Molestie Scelerisque Ltda'),
	(35, '40941590936931', 'Ante Arcu Ltda'),
	(36, '10674449044173', 'Dictum Eros & Cia'),
	(37, '57966662572988', 'Et Cras Ltda'),
	(38, '27021606072496', 'Proin Est & Cia'),
	(39, '83408167585394', 'Metus Luctus & Cia'),
	(40, '64378768526828', 'Ex Finibus S/A'),
	(41, '43836205168387', 'Tempor Ut Ltda'),
	(42, '75303146949204', 'Ac Odio & Cia');
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;

DELETE FROM `empresa_socio`;
/*!40000 ALTER TABLE `empresa_socio` DISABLE KEYS */;
/*!40000 ALTER TABLE `empresa_socio` ENABLE KEYS */;

DELETE FROM `socio`;
/*!40000 ALTER TABLE `socio` DISABLE KEYS */;
INSERT INTO `socio` (`id`, `cpf`, `nome`, `email`) VALUES
	(1, '05163432552', 'Lorem Iaculis', 'lorem@iaculis.com'),
	(2, '80902915953', 'Ipsum Scelerisque', 'ipsum@scelerisque.com'),
	(3, '76905753003', 'Dolor Diam', 'dolor@diam.com'),
	(4, '90988838594', 'Sit Tincidunt', 'sit@tincidunt.com'),
	(5, '03712788086', 'Amet Dapibus', 'amet@dapibus.com'),
	(6, '32260491667', 'Consectetur Pellentesque', 'consectetur@pellentesque.com'),
	(7, '96151709548', 'Adipiscing Eget', 'adipiscing@eget.com'),
	(8, '12447122735', 'Elit Vestibulum', 'elit@vestibulum.com'),
	(9, '38672916786', 'Fusce Magna', 'fusce@magna.com'),
	(10, '63636179853', 'At Sodales', 'at@sodales.com'),
	(11, '61413940904', 'Odio Vitae', 'odio@vitae.com'),
	(12, '74783907420', 'Luctus Mi', 'luctus@mi.com'),
	(13, '40381825199', 'Posuere Scelerisque', 'posuere@scelerisque.com'),
	(14, '24830464930', 'Dui Porta', 'dui@porta.com'),
	(15, '22316753684', 'Eget Mauris', 'eget@mauris.com'),
	(16, '93493475816', 'Rutrum Libero', 'rutrum@libero.com'),
	(17, '29254782078', 'Sapien Lectus', 'sapien@lectus.com'),
	(18, '07228369031', 'Sed A', 'sed@a.com'),
	(19, '13624281148', 'Pellentesque Vivamus', 'pellentesque@vivamus.com'),
	(20, '49311187822', 'Vulputate Scelerisque', 'vulputate@scelerisque.com'),
	(21, '97739315078', 'Dolor Sagittis', 'dolor@sagittis.com'),
	(22, '62530080642', 'Eu Turpis', 'eu@turpis.com'),
	(23, '65882823681', 'Porta Semper', 'porta@semper.com'),
	(24, '50243246634', 'Phasellus Sapien', 'phasellus@sapien.com'),
	(25, '71131981851', 'Dapibus Id', 'dapibus@id.com'),
	(26, '13022618718', 'Eleifend Vestibulum', 'eleifend@vestibulum.com'),
	(27, '34772710840', 'Neque Malesuada', 'neque@malesuada.com'),
	(28, '18903401151', 'Ac Dui', 'ac@dui.com'),
	(29, '85109390044', 'Dictum Facilisis', 'dictum@facilisis.com'),
	(30, '00504471925', 'Ut A', 'ut@a.com'),
	(31, '39910330750', 'Sed Ullamcorper', 'sed@ullamcorper.com'),
	(32, '48108977430', 'Massa Elit', 'massa@elit.com'),
	(33, '17746837338', 'Non Vestibulum', 'non@vestibulum.com'),
	(34, '36340449227', 'Dui Arcu', 'dui@arcu.com'),
	(35, '47088848241', 'Sollicitudin Tincidunt', 'sollicitudin@tincidunt.com'),
	(36, '23872393401', 'Elementum Etiam', 'elementum@etiam.com'),
	(37, '18713193249', 'Sit Justo', 'sit@justo.com'),
	(38, '59152959101', 'Amet Pellentesque', 'amet@pellentesque.com'),
	(39, '76713519226', 'Vel At', 'vel@at.com'),
	(40, '88512882438', 'Velit Eros', 'velit@eros.com'),
	(41, '78102652740', 'In Porttitor', 'in@porttitor.com'),
	(42, '86315883791', 'Vel Id', 'vel@id.com'),
	(43, '83875698654', 'Venenatis Pellentesque', 'venenatis@pellentesque.com'),
	(44, '96251973123', 'Neque Euismod', 'neque@euismod.com'),
	(45, '39884126188', 'Donec Imperdiet', 'donec@imperdiet.com'),
	(46, '53628667593', 'Massa Et', 'massa@et.com'),
	(47, '08603681329', 'Tortor Ex', 'tortor@ex.com'),
	(48, '83597082850', 'Congue Volutpat', 'congue@volutpat.com'),
	(49, '88742779322', 'Ac Aenean', 'ac@aenean.com'),
	(50, '76258263542', 'Nunc Auctor', 'nunc@auctor.com'),
	(51, '86277513291', 'Sit Commodo', 'sit@commodo.com'),
	(52, '41462666430', 'Amet Justo', 'amet@justo.com'),
	(53, '59644311791', 'Imperdiet Ultricies', 'imperdiet@ultricies.com'),
	(54, '04125917224', 'Finibus Turpis', 'finibus@turpis.com'),
	(55, '37913879373', 'Ligula Eu', 'ligula@eu.com'),
	(56, '96455829039', 'Vestibulum Proin', 'vestibulum@proin.com'),
	(57, '59686219375', 'Vulputate Consequat', 'vulputate@consequat.com'),
	(58, '50493792892', 'Tortor Imperdiet', 'tortor@imperdiet.com'),
	(59, '48514604314', 'Urna Dui', 'urna@dui.com'),
	(60, '09628402556', 'Ut Amet', 'ut@amet.com'),
	(61, '11426292401', 'Accumsan Sit', 'accumsan@sit.com'),
	(62, '54084033735', 'Ipsum Enim', 'ipsum@enim.com'),
	(63, '17676406400', 'Commodo Non', 'commodo@non.com'),
	(64, '18549508176', 'Ac Nam', 'ac@nam.com');
/*!40000 ALTER TABLE `socio` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
