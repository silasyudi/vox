/**
 * Exibe uma janela de confirmacao para remocao de um socio de uma empresa
 * @param {string} nome
 * @param {int} id
 */
function removeSocioModal(nome, id) {
    $('#remove-socio .socio').text(nome);
    $('#delete-socio').data('socio', id);
    $('#remove-socio').modal('show');
}

/**
 * Remove um socio de uma empresa
 * @param {int} id O ID do socio a ser removido
 */
function confirmaRemoveSocio(id) {
    $('.ui.dimmer').addClass('active');
    $('.ui.loader').removeClass('disabled');
    $.ajax({
        url: path_empresa_remove_socio,
        type: 'POST',
        data: {
            socio: id
        }
    }
    ).done(function (r) {
        if (r.hasOwnProperty('error')) {
            $('.ui.dimmer').removeClass('active');
            $('.ui.loader').addClass('disabled');
        } else {
            window.location.reload();
        }
    }
    );
}

/**
 * Exibe uma janela de confirmacao para excluir uma empresa
 */
function excluiEmpresa() {
    $('#exclui-empresa').modal('show');
}